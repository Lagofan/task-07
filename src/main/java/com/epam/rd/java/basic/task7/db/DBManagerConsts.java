package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public abstract class DBManagerConsts {
	private DBManagerConsts() {
		
	}
	
	
	//SELECTS
	//users
	public static final String SELECT_ALL_USERS = "SELECT * FROM users ORDER BY id";
	public static final String SELECT_USER_BY_LOGIN = "SELECT * FROM users u WHERE u.login=? ORDER BY u.id";
	public static final String SELECT_ALL_TEAMS = "SELECT * FROM teams ORDER BY id";
	public static final String SELECT_TEAM_BY_NAME = "SELECT * FROM teams t WHERE t.name=? ORDER BY t.id";
	public static final String SELECT_TEAM_BY_ID = "SELECT * FROM teams t WHERE t.id=? ORDER BY t.id";
	public static final String SELECT_ALL_USERS_TEAMS = "SELECT team_id FROM users_teams WHERE user_id=?";
	
	//INSERTS
	public static final String INSERT_USER = "INSERT INTO users VALUES (DEFAULT, ?)";
	public static final String INSERT_TEAM = "INSERT INTO teams VALUES (DEFAULT, ?)";
	public static final String INSERT_TEAMS_FOR_USER = "INSERT INTO users_teams VALUES (?, ?)";
	
	//DELETE
	public static final String DELETE_TEAM = "DELETE FROM teams WHERE name=?";
	public static final String DELETE_USER = "DELETE FROM users WHERE login=?";
	
	//UPDATE
	public static final String UPDATE_TEAM = "UPDATE teams SET name=? WHERE id=?";

}
