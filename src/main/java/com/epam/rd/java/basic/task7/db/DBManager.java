package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	private static final String APP_PROPS_FILE = "app.properties";
    public static String FULL_URL;

    static {
        try {
            FULL_URL = Files.readString(Path.of(APP_PROPS_FILE)).replace("connection.url=", "");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	public static synchronized DBManager getInstance() {
		return new DBManager();
	}
	
	
	private DBManager() {
	}
	
	
	public boolean deleteUsers(User... users) throws DBException {
		Connection conn = null;
		PreparedStatement st = null;
		try{
			conn = DriverManager.getConnection(FULL_URL);
			conn.setAutoCommit(false);
			st = conn.prepareStatement(DBManagerConsts.DELETE_USER);
			for(User user: users) {
				st.setString(1, user.getLogin());
				st.executeUpdate();
			}
			conn.commit();
			return true;
		}catch(NullPointerException e) {
			return false;
		}catch(SQLException ex) {
			rollback(conn);
			logSQLException(ex);
			return false;
		}finally {
			close(conn);
			close(st);
		}
	}
	
	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection conn = null;
		PreparedStatement st = null;
		try{
			conn = DriverManager.getConnection(FULL_URL);
			conn.setAutoCommit(false);
			st = conn.prepareStatement(DBManagerConsts.INSERT_TEAMS_FOR_USER, Statement.RETURN_GENERATED_KEYS);
			for(Team team: teams) {
				st.setInt(1, user.getId());
				st.setInt(2, team.getId());
				st.executeUpdate();
			}
			conn.commit();
			return true;
		}catch(NullPointerException e) {
			return false;
		}catch(SQLException ex) {
			rollback(conn);
			logSQLException(ex);
			return false;
		}finally {
			close(conn);
			close(st);
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> res = new ArrayList();
		Connection conn = null;
		PreparedStatement st = null;
		try{  
			conn = DriverManager.getConnection(FULL_URL);
			st = conn.prepareStatement(DBManagerConsts.SELECT_ALL_USERS_TEAMS);
			st.setInt(1, user.getId());
			try(ResultSet rs = st.executeQuery();){
	        	while(rs.next()) {
	        		res.add(this.getTeam(rs.getInt("team_id")));
	        	}
	        }
        }catch(NullPointerException e) {
        	e.printStackTrace();
		}catch (SQLException ex) {
            logSQLException(ex);
        }finally {
			close(conn);
			close(st);
		}
		return res;
	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection conn = null;
		PreparedStatement st = null;
		try{
			conn = DriverManager.getConnection(FULL_URL);
			conn.setAutoCommit(false);
			st = conn.prepareStatement(DBManagerConsts.DELETE_TEAM);
			st.setString(1, team.getName());
			st.executeUpdate();
			conn.commit();
			return true;
		}catch(NullPointerException e) {
			return false;
		}catch(SQLException ex) {
			rollback(conn);
			logSQLException(ex);
			return false;
		}finally {
			close(conn);
			close(st);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection conn = null;
		PreparedStatement st = null;
		try{
			conn = DriverManager.getConnection(FULL_URL);
			conn.setAutoCommit(false);
			st = conn.prepareStatement(DBManagerConsts.UPDATE_TEAM);
			st.setString(1, team.getName());
			st.setInt(2, team.getId());
			st.executeUpdate();
			conn.commit();
			return true;
		}catch(NullPointerException e) {
			return false;
		}catch(SQLException ex) {
			rollback(conn);
			logSQLException(ex);
			return false;
		}finally {
			close(conn);
			close(st);
		}
	}
	

	public List<User> findAllUsers() throws DBException{
		List<User> res = new ArrayList();
		try (Connection conn = DriverManager.getConnection(FULL_URL);
			Statement st = conn.createStatement();
	        ResultSet rs = st.executeQuery(DBManagerConsts.SELECT_ALL_USERS);){  
            while(rs.next()) {
            	res.add(mapUser(rs));
            }
        } catch (SQLException ex) {
            logSQLException(ex);
        }
		return res;
	}

	public boolean insertUser(User user) throws DBException {
		boolean flag = false;
		Connection conn = null;
		PreparedStatement st = null;
		try{
			conn = DriverManager.getConnection(FULL_URL);
			conn.setAutoCommit(false);
			conn.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			st = conn.prepareStatement(DBManagerConsts.INSERT_USER, Statement.RETURN_GENERATED_KEYS);
			st.setString(1, user.getLogin());
			int count = st.executeUpdate();
			if (count > 0) {
				try(ResultSet rs = st.getGeneratedKeys();){
					if(rs.next()) {
						user.setId(rs.getInt(1));
					}
				}
				flag = true;
			}
			conn.commit();
	    }catch(NullPointerException e) {
			e.printStackTrace();
		}catch (SQLException ex) {
	    	rollback(conn);
	        logSQLException(ex);
	    }finally {
	    	close(conn);
	    	close(st);
	    }
		return flag;
	}

	public User getUser(String login) throws DBException {
		User res = null;
		try (Connection conn = DriverManager.getConnection(FULL_URL);
			PreparedStatement st = conn.prepareStatement(DBManagerConsts.SELECT_USER_BY_LOGIN);){  
			st.setString(1, login);
			 try(ResultSet rs = st.executeQuery();){
		        	if(rs.next()) {
		        		res = mapUser(rs);
		        	}
		     }
	    } catch (SQLException ex) {
	    	logSQLException(ex);
	    }
		return res;
	}

	public Team getTeam(String name) throws DBException {
		Team res = null;
		try (Connection conn = DriverManager.getConnection(FULL_URL);
			PreparedStatement st = conn.prepareStatement(DBManagerConsts.SELECT_TEAM_BY_NAME);){  
			st.setString(1, name);
	        try(ResultSet rs = st.executeQuery();){
	        	if(rs.next()) {
	        		res = mapTeam(rs);
	        	}
	        }
	    } catch (SQLException ex) {
	    	logSQLException(ex);
	    }
		return res;
	}
	
	public Team getTeam(int id) throws DBException {
		Team res = null;
		try (Connection conn = DriverManager.getConnection(FULL_URL);
			PreparedStatement st = conn.prepareStatement(DBManagerConsts.SELECT_TEAM_BY_ID);){  
			st.setInt(1, id);
	        try(ResultSet rs = st.executeQuery();){
	        	if(rs.next()) {
	        		res = mapTeam(rs);
	        	}
	        }
	    } catch (SQLException ex) {
	    	logSQLException(ex);
	    }
		return res;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> res = new ArrayList();
		try (Connection conn = DriverManager.getConnection(FULL_URL);
				Statement st = conn.createStatement();
		        ResultSet rs = st.executeQuery(DBManagerConsts.SELECT_ALL_TEAMS);){  
	            while(rs.next()) {
	            	res.add(mapTeam(rs));
	            }
	        } catch (SQLException ex) {
	            logSQLException(ex);
	        }
			return res;
	}

	public boolean insertTeam(Team team) throws DBException{
		boolean flag = false;
		Connection conn = null;
		PreparedStatement st = null;
		try{
			conn = DriverManager.getConnection(FULL_URL);
			conn.setAutoCommit(false);
			st = conn.prepareStatement(DBManagerConsts.INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);
			st.setString(1, team.getName());
			int count = st.executeUpdate();
			if (count > 0) {
				try(ResultSet rs = st.getGeneratedKeys();){
					if(rs.next()) {
						team.setId(rs.getInt(1));
					}
				}
				flag = true;
			}
			conn.commit();
	    }catch(NullPointerException e) {
			e.printStackTrace();
		}catch (SQLException ex) {
	    	rollback(conn);
	        logSQLException(ex);
	    }finally {
	    	close(conn);
	    	close(st);
	    }
		return flag;
	}
	
	
	private void rollback(Connection conn) throws DBException {
		try {
			conn.rollback();
		}catch(SQLException e) {
			logSQLException(e);
		}
	}
	
	private User mapUser(ResultSet rs) throws SQLException {
		User user = new User();
		user.setId(rs.getInt("id"));
		user.setLogin(rs.getString("login"));
		return user;
	}
	
	private Team mapTeam(ResultSet rs) throws SQLException {
		Team team = new Team();
		team.setId(rs.getInt("id"));
		team.setName(rs.getString("name"));
		return team;
	}
	
	
	private void logSQLException(SQLException ex) throws DBException {
		throw new DBException("SQLException: " + ex.getMessage() + '\n' +
							  "SQLState: " + ex.getSQLState() + '\n' + 
							  "VendorError: " + ex.getErrorCode(), ex);
	}
	
	private void close(AutoCloseable conn) throws DBException {
		try {
			if(conn != null) {
				conn.close();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}


}
